    import Foundation
    import XCTest
    @testable import JustResourcesSLib

    final class JustResourcesSLibTests: XCTestCase {
        func testExample() {
            // This is an example of a functional test case.
            // Use XCTAssert and related functions to verify your tests produce the correct
            // results.
            XCTAssertEqual(JustResourcesSLib().name, "JustResourcesSLib")
        }
    }
