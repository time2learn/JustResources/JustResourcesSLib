import Foundation

struct JustResourcesSLib {
    var name = "JustResourcesSLib"
    
    static func path() -> String {
        return Bundle.main.bundlePath
    }
}
